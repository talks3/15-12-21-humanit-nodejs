import express from "express";
import bodyParser from "body-parser";

const app = express();

app.post(
	"/v1/um-endpoint-random",
	bodyParser.json(),
	(req, res) => {
		console.log(req.body);

		const startTime = Date.now();
		while (Date.now() - startTime < 5 * 1000) { }

		res.status(200).send(req.body);
	}
);

app.listen(8080, () =>
	console.log("Siga 💪")
);

console.log("Executamos isto tudo até aqui.");
