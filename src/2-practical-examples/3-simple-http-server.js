import express from "express";
import bodyParser from "body-parser";

const app = express();

app.post(
	"/v1/um-endpoint-random",
	bodyParser.json(),
	(req, res) => {
		console.log(req.body);

		setTimeout(
			() => res.status(200).send({ ...req.body }),
			10 * 1000
		);
	}
);

app.listen(8080, () =>
	console.log("Siga 💪")
);

console.log("Executamos isto tudo até aqui.");
