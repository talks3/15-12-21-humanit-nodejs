# Como foi o setup ?

1. vercel.com -> start deploying
2. https://vercel.com/docs/concepts/functions/serverless-functions
3. Criar ./src/api/v1/um-endpoint-random/index.ts
4. `npm i --save next`
5. `npm i --save-dev vercel`
6. Procurar uma maneira de fazer deployment pelo terminal
7. https://vercel.com/docs/cli
8. Criar conta no vercel
9. `npx vercel`
10. Login no cli, escolhar a conta, não  fazer link a um projeto que existe, project name: humanit-node-talk, código em ./src
11. `curl -X POST LINK-NO-CLIPBOARD/v1/um-endpoint-random -H "Content-Type: application/json" -d '{"pedido":1}'`
12. `The page could not be found NOT_FOUND` 🙃
13. dashboard do vercel -> projetos -> humanit-node-talk -> View function logs -> all Paths -> api/v1/um-endpoint-random/index.ts ?
14. `curl -X POST LINK-NO-CLIPBOARD/api/v1/um-endpoint-random/index.ts -H "Content-Type: application/json" -d '{"pedido":1}'`
15. `> {"pedido":1}` hmmmm... Não era bem isto
16. `curl -X POST LINK-NO-CLIPBOARD/api/v1/um-endpoint-random -H "Content-Type: application/json" -d '{"pedido":1}'`
17. `> {"pedido":1}` Good enough
18. dashboard do vercel -> projetos -> humanit-node-talk -> View function logs -> all Paths -> api/v1/um-endpoint-random/index.ts
19. `Awaiting new requests…` no logs ?

# Próximos passos

* Custom domains
* Tentar perceber porque é que os logs não estão a aparecer
* Git CI/CD integration
* Analytics
* Secrets
* Fazer um cházinho para acalmar os nervos que vêem da quantidade de "opinação" de next/nuxt e vercel
