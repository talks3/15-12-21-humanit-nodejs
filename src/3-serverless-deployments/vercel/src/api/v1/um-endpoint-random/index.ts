import { NextApiRequest, NextApiResponse } from 'next';

export default (req: NextApiRequest, res: NextApiResponse) =>{
	console.log(req.body);

	return new Promise(resolve => {
		setTimeout(
			() => resolve(res.status(200).json({
				...req.body
			})),
			2 * 1000
		);
	});
}
