import {APIGatewayProxyHandlerV2} from "aws-lambda";

export const handler: APIGatewayProxyHandlerV2 = event => {
	console.log(event.body);

	return new Promise(resolve => {
		setTimeout(
			() => resolve({
				statusCode: 200,
				headers: {},
				body: event.body
			}),
			2 * 1000
		);
	});
};