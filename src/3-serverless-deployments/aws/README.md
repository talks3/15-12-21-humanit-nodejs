# Como foi o setup ?

1. searx.be: getting started cdk serverless
2. https://docs.aws.amazon.com/cdk/latest/guide/serverless_example.html
3. `npx cdk init app --language typescript`
4. Apaguar ficheiros de testes (jest), isso fica para outra talk 😉
5. Mudar o nome do projecto no package.json
6. Mudar o nome da stack no bin/aws.js
7. Criar conta na aws
8. Login na aws
9. Conta -> Security Credentials -> Access Keys -> Sacar o ficheiro com as chaves
10. Instalar aws cli
11. `aws configure --profile um-nome-de-prefil` (por agora vai ser humanit), e meter as chaves do ficheiro do passo 9. com região eu-west-1 (ou outra qualquer)
14. Ler https://docs.aws.amazon.com/cdk/latest/guide/serverless_example.html de cima a baixo
15. Adicionar o script de bootstrap e deploy ao package.json
16. Andar à procura na net de uma maneira melhor de fazer `if (method === 'POST') `
17. https://docs.aws.amazon.com/cdk/api/latest/docs/aws-apigateway-readme.html #OpenAPI Definition
18. Criar um spec em open api em lib/api-spec.yml
19. `npm i --save-dev @aws-cdk/aws-apigateway @aws-cdk/aws-lambda @aws-cdk/aws-lambda-nodejs @aws-cdk/aws-iam @types/aws-lambda` cenas de cdk que são precisas + uns typings para typescript
20. Adicionar um lambda e apigateway ao lib/aws-stack.ts
21. Criar um handler em src/pingPongHandler.ts
22. searx.be: cdk lambda allow invoke apigateway
23. https://stackoverflow.com/questions/62201988/aws-cdk-how-to-grant-invoke-permissions-on-a-lambda-to-api-gateway-before-depl
24. `npm run cdk -- synth`
25. Tentar arranjar uma maneira para isto não usar docker para fazer bundle
26. search.be: nodejs function lambda cdk
27. https://docs.aws.amazon.com/cdk/api/latest/docs/aws-lambda-nodejs-readme.html ctrl+f `docker`
28. `If esbuild is available it will be used to bundle your code in your environment. Otherwise, bundling will happen in (blah blah blah)`
29. `npm i --save-dev esbuild`
30. `npm run cdk -- synth`
31. Profit!
32. Voltar ao link do passo 14. para ver como se faz deployment disto
33. `npm run deploy` -> y
34. Let's go!
35. AWS UI (eu-west-1 ou outra que do passo 11.) -> cloudformation -> stacks -> HumanITNodejsTalk -> resources -> Logical id `ApiGateway{uns numeros e letras random}` -> abrir link no `physical id` tab -> POST -> TEST -> Request body: `{"pedido": "cenas"}` -> Test
36. Done!
37. `curl -X POST LINK-QUE-APARECE-NO-FIM-DO-NPM-RUN-DEPLOY/v1/um-endpoint-random -H "Content-Type: application/json" -d '{"pedido":1}'`
38. `> {"pedido":1}` 🚀

Bonus:
39. Como é provavel que alguem tente fazer um DoS no link que vai aparecer na talk -> searx.be: apigateway rate limit cdk
40. https://docs.aws.amazon.com/cdk/api/latest/docs/@aws-cdk_aws-apigateway.ThrottleSettings.html
41. Mudar o lib/aws-stack.ts para ter o `addUsagePlan`
42. `npm run deploy`
43. `wrk -c 5 -d 10s -s post.lua LINK-QUE-APARECE-NO-FIM-DO-NPM-RUN-DEPLOY/v1/um-endpoint-random` com um .lua daqui `https://stackoverflow.com/questions/15261612/post-request-with-wrk/26896415#26896415`
44. lambda -> invocations = 440, concurrent executions = 2. Ainda um pouco alto
45. searx.be: aws throttle lambda
46. https://docs.aws.amazon.com/lambda/latest/dg/configuration-concurrency.html
47. Mudar o lib/aws-stack.ts para ter `reservedConcurrentExecutions: 1`
48. `npm run deploy`
49. `Specified ReservedConcurrentExecutions for function decreases account's UnreservedConcur
rentExecution below its minimum value of` -> searx.be
50. No dice. Pls don't spam 🙏

# Próximos passos

* Custom domains
* Git CI/CD integration
* Multi region deployment
* Web application firewall (waf)
* Lambda concurrency limit
* Secret manager
* async lambdas (sqs, sns, event bridge)
* Observability with xray
* Networking (vpc, subnets, routing tables, security groups, internet gateways)
