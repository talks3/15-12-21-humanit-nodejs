import * as cdk from '@aws-cdk/core';
import {NodejsFunction} from '@aws-cdk/aws-lambda-nodejs';
import {Architecture, Runtime} from '@aws-cdk/aws-lambda';
import {ApiDefinition, RestApi, SpecRestApi} from '@aws-cdk/aws-apigateway';
import {ServicePrincipal} from '@aws-cdk/aws-iam';
import {join} from 'path';
// import * as sqs from '@aws-cdk/aws-sqs';

export class AwsStack extends cdk.Stack {
  pingPongLambda: NodejsFunction;
  apiGateway: SpecRestApi;

  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    this.pingPongLambda = new NodejsFunction(this, "PingPong", {
      runtime: Runtime.NODEJS_14_X,
      functionName: "PingPongFunction",
      architecture: Architecture.ARM_64,
      memorySize: 128,
      retryAttempts: 0,
      entry: join(__dirname, '..', 'src', 'pingPongHandler.ts'),
    });

    this.apiGateway = new SpecRestApi(this, 'ApiGateway', {
      apiDefinition: ApiDefinition.fromAsset('lib/api-spec.yml')
    });
    this.pingPongLambda.grantInvoke(new ServicePrincipal('apigateway.amazonaws.com'));
  }
}
